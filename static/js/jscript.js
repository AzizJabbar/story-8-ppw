$("#myform").submit(function(){
    var search = $("#books").val();
    if(search == ""){
        alert("Please enter something in the field");
    }
    else{		
        var url = '/data?q=' + search;
        $.ajax({
            url : url,
            success: function(response){
                $('#result').empty();
                for(i=1;i<=response.items.length;i++){
                    var row1 = $('<tr>');
                    var number = $('<th scope="row">' + i + '</th>');     
                    var img = $('<td><img class="img-fluid img-thumbnail" src="'+response.items[i].volumeInfo.imageLinks.thumbnail+'"></td>');
                    var title = $('<td>' + response.items[i].volumeInfo.title + '</td>');
                    var author = $('<td>' + response.items[i].volumeInfo.authors + '</td>');
                    var pageCount = $('<td>' + response.items[i].volumeInfo.pageCount + '</td>');
                    row1.appendTo('#result');
                    number.appendTo('#result');
                    img.appendTo('#result');
                    title.appendTo('#result');
                    author.appendTo('#result');
                    pageCount.appendTo('#result');
                }
            }
        })
    }
    return false;
});