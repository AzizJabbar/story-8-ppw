from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home

class TestIndex(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_index_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_data_is_exist(self):
        response = Client().get('/data/?q=holmes')
        self.assertEqual(response.status_code, 200)
